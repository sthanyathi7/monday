import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class SessionsList extends StatefulWidget {
  const SessionsList({super.key});

  @override
  State<SessionsList> createState() => _SessionsListState();
}

class _SessionsListState extends State<SessionsList> {
  final Stream<QuerySnapshot> _mySessions =
      FirebaseFirestore.instance.collection("sessions").snapshots();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _mySessions,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          const SnackBar(
            content: Text('Something went wrong'),
          );
          return const Text("Something went wrong");
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        }

        if (snapshot.hasData) {
          return Padding(
            padding: const EdgeInsets.all(24.0),
            child: Row(
              children: [
                Expanded(
                    child: SizedBox(
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width,
                        child: ListView(
                          children: snapshot.data!.docs
                              .map((DocumentSnapshot documentSnapshot) {
                            Map<String, dynamic> data =
                                documentSnapshot.data() as Map<String, dynamic>;

                            return ListTile(
                              title: Text(data['name']),
                              subtitle: Text(data['surname']),
                              trailing: Text(data['province']),
                            );
                          }).toList(),
                        )))
              ],
            ),
          );
        } else {
          const SnackBar(
            content: Text('No Data'),
          );
          return const Text("No Data");
          ;
        }
      },
    );
  }
}
